#!/bin/sh
# 
# BOOT up script to program the VIRTEX 
#
# --- Set some defaults ----------------------------------------------
RCUFW_READBACK=1
RCUFW_FROM_FLASH=1

# --- Try to identify the card ---------------------------------------
mach=`ifconfig | sed -n -e 's/.*HWaddr //p' | sed -e 's/ *$//'`
case $mach in
    *01:45) SUBDET=FMD0 ;; 
    *01:0E) SUBDET=FMD0 ;; 
    *01:0F) SUBDET=FMD1 ;;
    *01:10) SUBDET=FMD2 ;;
    *01:11) SUBDET=FMD3 ;;
    *)      SUBDET=              ;;
esac

# --- Source possible configuration ----------------------------------
if test -f /home/dcs/etc/sysconfig/rcufw ; then 
    . /home/dcs/etc/sysconfig/rcufw
fi
if test -f /home/dcs/etc/sysconfig/rcufw.${SUBDET} ; then 
    . /home/dcs/etc/sysconfig/rcufw.${SUBDET}
fi
    

# since the script might be called from the rcS the PATH 
# of the rcu-sh has to be added
export PATH=/usr/local/sbin:$PATH

# --- Set the DCS to RCU clock to 40 MHz -----------------------------
echo "FFFFF7">/dev/vreg

# --- Added line needed after ACTEL fw version 2.3 -------------------
rcu-sh w 0xb112 0xa 

# ---- Program the RCU via select map --------------------------------
rcu-sh sm e

bitdirs="/home/dcs/lib/firmware \
 /dcs-share/bit_files \
 /rcu
 "

ok=0
for i in $bitdirs ; do 
    f=$i/rcu.bit
    if test ! -e $f ; then 
	continue 
    fi
    echo -n "Programming RCU configuration from $f ... "
    cat $f > /dev/virtex 
    ret=$? 
    if test $ret -ne 0 ; then 
	echo "failed" 
	continue
    fi
    ok=1
    echo "done"
    break
done 
if test $ok -ne 1 ; then 
    echo "No valid RCU configuration file found" 
fi

rcu-sh sm d
# Enable TTCrx chip for SERB
# echo -n "Enabling TTCrx for SERB ... "
# ttcrx_regs w 0x7f 3
# test $? -ne 0 && echo "failed" || echo "done"

sleep 1
if test $RCUFW_FROM_FLASH -gt 0 ; then 
# Program the RCU from the FLASH 
    echo "Program RCU from flash"
    rcu-sh w 0xb000 0x4 
    sleep 1 
fi
if test $RCUFW_READBACK -gt 0 ; then 
    # Reset counters in Actel
    echo "Reset status and counters in Actel"
    rcu-sh w 0xb000 0x2 
    sleep 1
    # Check registers before turning on READBACK
    echo "Register before turning on READBACK"
    rcu-sh r 0xb104 "Number of SEU:             %d"
    rcu-sh r 0xb105 "Last frame in error:       0x%x"
    rcu-sh r 0xb106 "Last frame read:           0x%x"
    rcu-sh r 0xb107 "Number of complete cycles: %d"
    # Write 0 to data register - infinite READBACK
    rcu-sh w 0xb103 0x0
    sleep 1 
    # Enable READBACK
    echo "Turn on READBACK"
    rcu-sh w 0xb000 0x200
    sleep 1
    # Check registers after turning on READBACK
    echo "Register after turning on READBACK"
    rcu-sh r 0xb104 "Number of SEU:             %d"
    rcu-sh r 0xb105 "Last frame in error:       0x%x"
    rcu-sh r 0xb106 "Last frame read:           0x%x"
    rcu-sh r 0xb107 "Number of complete cycles: %d"
fi
echo "Read the status"
echo "[11:8]: Stop power, [7]: isNull, [6]: Busy, [5]: unused, [4]: SMAP busy, [3:0]: Last active code"
rcu-sh r 0xb100 "Status:                    0x%x"

# 
# EOF
#


