#include <fstream>
#include <string>
#include <iostream>
#include <cctype>
#include <iomanip>

void
printChar(size_t addr, char c)
{
  std::cout << std::setw(8) << addr << ": " << c << std::endl;
}

int
main(int argc, char** argv)
{
  std::string inFileName("rcu-20101130.bit");
  if (argc > 1) inFileName = argv[1];
  
  std::ifstream in(inFileName.c_str());
  if (!in) { 
    std::cerr << "Failed to open " << inFileName << std::endl;
    return 1;
  }

  // Now read a char at a time, and check if it is printable, 
  // and give the offset 
  size_t addr = 0;
  size_t saddr = 0;
  std::string str = "";
  while (!in.eof()) { 
    char c = in.get();
    
    if (isalnum(c) || ispunct(c)) { 
      if (saddr == 0) saddr = addr;
      str.push_back(c);
      printChar(addr, c);
    }
    else { 
      printChar(addr, c);
      if (saddr > 0) { 
	std::cout << "String at " << saddr << ": " << str << std::endl;
      }
      saddr = 0;
      str   = "";
    }
    addr++;
  } 
  
  return 0;
}

 
