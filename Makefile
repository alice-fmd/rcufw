#
#
#
NAME		=  rcufw
#VERSION		:= $(shell strings rcu.bit | sed -n '3p' | tr -d '/'
CURRENT		:= $(shell ls rcu-*.bit  | sort -n -r | sed -e 's/rcu-//' -e 's/\.bit//'  | head -n 1)
YEAR		:= $(shell echo $(VERSION) | sed 's/\(....\).....*/\1/')
MONTH		:= $(shell echo $(VERSION) | sed 's/....\(..\)...*/\1/')
DAY		:= $(shell echo $(VERSION) | sed 's/......\(..\).*/\1/')
SUB		:= $(shell echo $(VERSION) | sed 's/........\(.*\)/\1/')
MVERS		:= $(shell ./vers2mvers $(VERSION))
MANVERS		:= $(strip $(MVERS))
PROGRAMS	:= framegen rcuflashprog 	
PREFIX		:= /home/dcs
SCRIPTS		:= dump_rcu_regs.sh		\
		   rcufw_program		\
		   rcufw_gen_frames		
DATA		:= read_header.txt 		\
		   read_footer.txt		\
		   write_header.txt 		\
		   write_footer.txt		\
		   frames936.txt	
SOURCES		=  Makefile			\
		   S30rcu-conf.sh 		\
		   README			\
		   rcufw.sysconfig		\
		   rcu-$(VERSION).bit 		\
		   manual-$(MANVERS).pdf 	\
		   $(NAME).spec.in		\
		   vers2mvers			
SPEC		=  $(NAME).spec
LVL		=
RELEASE		=
RPMBUILD	=  rpmbuild
# RPMFLAGS	=  --define "_topdir `pwd`/rpms"
ifdef NOLOCAL
RPMBUILD	= sudo rpmbuild
RPMFLAGS	= 
endif
ifeq ($(RELEASE), )
RELEASE		= 1
endif
ifeq ($(LVL), )
LVL		= -stable
endif

all:
	@echo "Nothing to be done for $@" 

install: 
	chmod a+x dump_rcu_regs.sh
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	mkdir -p $(DESTDIR)$(PREFIX)/etc/init.d
	mkdir -p $(DESTDIR)$(PREFIX)/etc/sysconfig
	mkdir -p $(DESTDIR)$(PREFIX)/lib/firmware/$(VERSION)
	mkdir -p $(DESTDIR)$(PREFIX)/var/lib/firmware/$(VERSION)
	mkdir -p $(DESTDIR)$(PREFIX)/share/doc/rcufw-$(VERSION)
	for i in $(PROGRAMS) ; do \
	  	cp $$i $(DESTDIR)$(PREFIX)/bin/ ; done 
	for i in $(SCRIPTS) ; do \
		cp $$i $(DESTDIR)$(PREFIX)/bin/ ; done 
	for i in $(DATA) ; do \
		cp $$i $(DESTDIR)$(PREFIX)/lib/firmware/ ; done 
	chmod a+x $(DESTDIR)$(PREFIX)/bin/*
	cp -a S30rcu-conf.sh        $(DESTDIR)$(PREFIX)/etc/init.d/
	cp -a rcufw.sysconfig        $(DESTDIR)$(PREFIX)/etc/sysconfig/rcufw
	cp -a rcu-$(VERSION).bit    \
		$(DESTDIR)$(PREFIX)/lib/firmware/$(VERSION)/rcu.bit
	cp -a README   		    \
		$(DESTDIR)$(PREFIX)/share/doc/rcufw-$(VERSION)/
	cp -a manual-$(MANVERS).pdf \
		$(DESTDIR)$(PREFIX)/share/doc/rcufw-$(VERSION)/manual.pdf
	rm -f $(DESTDIR)$(PREFIX)¼/bin/*.debug

show:	
	@echo "VERISON: $(VERSION)"
	@echo "CURRENT: $(CURRENT)"
	@echo "MANVERS: $(MANVERS)"
	@echo "YEAR:    $(YEAR)"
	@echo "MONTH:   $(MONTH)"
	@echo "DAY:     $(DAY)"
	@echo "SUB:     $(SUB)"
	@echo "SPEC:    $(SPEC)"
	@echo "NAME:	$(NAME)"

dist:	$(NAME)-$(VERSION).tar.gz

clean:
	rm -f $(NAME)-$(VERSION).tar.gz
	rm -f $(SPEC)
	rm -f $(NAME)-*.tar.gz

rpm-nosu:dist
	rpmbuild -D "_topdir $(HOME)/rpm" -ta $(distdir).tar.gz

rpm:	dist
	sudo rpmbuild $(RPMFLAGS) -ta $(NAME)-$(VERSION).tar.gz

all-rpm:
	@for i in rcu-*.bit ; do \
	  rm -rf $(SPEC) ; \
	  v=`echo $$i | sed -e 's/rcu-//' -e 's/\.bit//'` ; \
	  $(MAKE) rpm VERSION=$$v ; done
	$(MAKE) current-rpm 

current-rpm:
ifeq ($(CURRENT), )
	@echo "CURRENT not defined" 
else
	$(MAKE) rpm VERSION=$(CURRENT) LVL=-unstable
endif

$(SPEC):$(SPEC).in
	sed -e 	's/@VERSION@/$(VERSION)/g'	\
	    -e  's/@LVL@/$(LVL)/g'		\
	    -e  's/@RELEASE@/$(RELEASE)/g'	\
	    -e  's/@NAME@/$(NAME)/g'		\
		< $< > $@ 

$(NAME)-$(VERSION).tar.gz: $(SOURCES) $(PROGRAMS) $(SCRIPTS) $(DATA) $(SPEC)
	mkdir -p $(NAME)-$(VERSION)
	cp $^ $(SPEC) $(NAME)-$(VERSION) 
	tar -czvf $(NAME)-$(VERSION).tar.gz $(NAME)-$(VERSION)
	rm -rf $(NAME)-$(VERSION)

.PHONY:$(SPEC)

show_man:
	@echo $(MANVERS) 

#
# EOF
#
