#!/bin/sh
#
# A script to dump the content of all registers on the RCU to standard out. 
#
# MUST be executed on DCSC 
# 
RCU_SH=/usr/local/sbin/rcu-sh
if test -x $RCU_SH ; then 
    echo "rcu-sh not found - are you on the DCSC?" > /dev/stderr 
    exit 1
fi
echo "Reading RCU status Registers" 
echo -n "Firmware version:             "
$RCU_SH r 0x8008  
echo -n "Active FECs:                  "
$RCU_SH r 0x5100 
echo -n "ALTRO interface:              "
$RCU_SH r 0x5101 
echo -n "Trigger configuration:        "
$RCU_SH r 0x5102 
echo -n "Read-out mode:                "
$RCU_SH r 0x5103 
echo -n "ALTRO configuration 1:        "
$RCU_SH r 0x5104 
echo -n "ALTRO configuration 2:        "
$RCU_SH r 0x5105 
echo -n "Firmware version:             "
$RCU_SH r 0x5106 
echo -n "Back-plane version:           "
$RCU_SH r 0x5107 
echo -n "RCU identifier:               "
$RCU_SH r 0x5108 
echo -n "ALTRO Bus A errors:           "
$RCU_SH r 0x5110 
echo -n "ALTRO Bus B errors:           "
$RCU_SH r 0x5111 
echo -n "Read-out errors:              "
$RCU_SH r 0x5112 
echo -n "ALTRO bus status:             "
$RCU_SH r 0x5114 
echo -n "ALTRO bus status (TRSF):      "
$RCU_SH r 0x5115 
echo -n "Bus busy:                     "
$RCU_SH r 0x5116 
echo -n "Multi-event buffer counter:   "
$RCU_SH r 0x511b 
echo -n "Software trigger counter:     "
$RCU_SH r 0x511c 
echo -n "Aux. trigger counter:         "
$RCU_SH r 0x511d 
echo -n "TTC Level 2 accept counter:   "
$RCU_SH r 0x511e 
echo -n "TTC Level 2 reject counter:   "
$RCU_SH r 0x511f 
echo -n "Data strobe (A) counter:      "
$RCU_SH r 0x5120 
echo -n "Data strobe (B) counter:      "
$RCU_SH r 0x5121 
echo -n "Transfer gate (A) counter:    "
$RCU_SH r 0x5122 
echo -n "Transfer gate (B) counter:    "
$RCU_SH r 0x5123 
echo -n "Acknowledge (A) counter:      "
$RCU_SH r 0x5124 
echo -n "Acknowledge (B) counter:      "
$RCU_SH r 0x5125 
echo -n "Control strobe (A) counter:   "
$RCU_SH r 0x5126 
echo -n "Control strobe (B) counter:   "
$RCU_SH r 0x5127 
echo -n "Last data block length (A):   "
$RCU_SH r 0x5128 
echo -n "Last data block length (B):   "
$RCU_SH r 0x5129 
echo -n "Address mismatches:           "
$RCU_SH r 0x512a 
echo -n "Data block length mismatches: "
$RCU_SH r 0x512b 
echo -n "Arbitrator state:             "
$RCU_SH r 0x512c 
echo -n "Read-out state:               "
$RCU_SH r 0x512d 
echo -n "Instruction sequencer state:  "
$RCU_SH r 0x512e 
echo -n "Event manager state:          "
$RCU_SH r 0x512f 
echo -n "Data assembler state:         "
$RCU_SH r 0x5130 
echo -n "TTC control:                  "
$RCU_SH r 0x4000 
echo -n "Region-of-intrest control 1:  "
$RCU_SH r 0x4002 
echo -n "Region-of-intrest control 2:  "
$RCU_SH r 0x4003 
echo -n "L1 latency window:            "
$RCU_SH r 0x4006 
echo -n "L2 latency window:            "
$RCU_SH r 0x4007 
echo -n "ROI latency window:           "
$RCU_SH r 0x4009 
echo -n "L1 Message latency window:    "
$RCU_SH r 0x400a 
echo -n "Pre-pulse counter:            "
$RCU_SH r 0x400b 
echo -n "Bunch crossing counter:       "
$RCU_SH r 0x400c 
echo -n "L0 counter:                   "
$RCU_SH r 0x400d 
echo -n "L1 counter:                   "
$RCU_SH r 0x400e 
echo -n "L1 message counter:           "
$RCU_SH r 0x400f 
echo -n "L2 accept counter:            "
$RCU_SH r 0x4010 
echo -n "L2 reject counter:            "
$RCU_SH r 0x4011 
echo -n "ROI counter                   "
$RCU_SH r 0x4012 
echo -n "Bunch counter:                "
$RCU_SH r 0x4013 
echo -n "Hamming coding counter:       "
$RCU_SH r 0x4016 
echo -n "Error counter:                "
$RCU_SH r 0x4017 
echo -n "Number of buffered events:    "
$RCU_SH r 0x4020 
echo -n "DAQ header word 1:            "
$RCU_SH r 0x4021 
echo -n "DAQ header word 2:            "
$RCU_SH r 0x4022 
echo -n "DAQ header word 3:            "
$RCU_SH r 0x4023 
echo -n "DAQ header word 4:            "
$RCU_SH r 0x4024 
echo -n "DAQ header word 5:            "
$RCU_SH r 0x4025 
echo -n "DAQ header word 6:            "
$RCU_SH r 0x4026 
echo -n "DAQ header word 7:            "
$RCU_SH r 0x4027 
echo -n "Event information:            "
$RCU_SH r 0x4028 
echo -n "Event information:            "
$RCU_SH r 0x4029 
echo -n "Unknown TTC register 1:       "
$RCU_SH r 0x4030 
echo -n "Unknown TTC register 1:       "
$RCU_SH r 0x4031 4
echo -n "Unknown TTC register 1:       "
$RCU_SH r 0x4035
echo -n "Unknown TTC register 1:       "
$RCU_SH r 0x4036 7
echo -n "Unknown TTC register 1:       "
$RCU_SH r 0x403d
echo "End-of-Dump"


#
# EOF
#
